from flask import Flask, request
import webexteamssdk
from webexteamsarchiver import WebexTeamsArchiver
from google.cloud import firestore
import json
# import uuid
import os
import shutil
import datetime
import tempfile
import constants

app = Flask(__name__)
app.secret_key = os.environ.get("APP_SECRET") or os.urandom(24)
app.permanent_session_lifetime = datetime.timedelta(hours=1)

# bot credentials (used to send msgs to the user via WxT)
bot_id = os.environ.get(constants.WT_BOT_ID_ENV)
bot_token = os.environ.get(constants.WT_BOT_TOKEN_ENV)

# Initialize firestore db
db = firestore.Client()
task_collection = db.collection(constants.TASK_COLLECTION)


class TaskMaxRetries(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return 'Too many retries: {}'.format(self.value)


@app.route("/backup_task_handler", methods=["POST"])
def backuptaskhandler():
    """Get the list of room_ids to be backed up and process them
    """
    # get task info
    task_name = request.headers.get('X-AppEngine-TaskName')
    task_retries = int(request.headers.get('X-AppEngine-TaskRetryCount'))

    print(f"Task name: {task_name} - Retries: {task_retries}")

    payload = request.get_data(as_text=True) or '{}'
    print(f"Payload: {payload}")

    payload_data = json.loads(payload)

    # extract data from the payload
    wxt_access_token = payload_data.get("access_token")
    room = payload_data.get("room_id")
    account_mail = payload_data.get("account_mail")
    person_id = payload_data.get("person_id")
    room_name = payload_data.get("room_name")
    team_id = payload_data.get("team_id")
    team_name = payload_data.get("team_name")

    # Init the APIs using the user's token
    wxt_api = webexteamssdk.WebexTeamsAPI(access_token=wxt_access_token)
    wxt_archiver = WebexTeamsArchiver(wxt_access_token)
    archive_format = "zip"

    # prepare to send message to the user using a bot token
    bot_api = webexteamssdk.WebexTeamsAPI(access_token=bot_token)

    # Add a task to Firebase
    # task_id = task_name # or str(uuid.uuid4())
    task_doc = task_collection.document(task_name)

    try:
        result = {
            "room_name": room_name
        }
            
        task_doc.set({
            "user_id": person_id,
            "user_mail": account_mail,
            "room_id": room,
            "room_name": room_name,
            "status": "submitted",
            "start_time": datetime.datetime.now(),
            "team_id": team_id,
            "team_name": team_name,
            "handler": "wxt-backup-handler",
            "retries": task_retries
        })

        if task_retries > constants.TASK_MAX_RETRIES:
            raise TaskMaxRetries(task_retries)
        
        # notify that the backup is starting
        start_msg = bot_api.messages.create(
            toPersonEmail=account_mail,
            markdown=(
                f"Preparing to backup room '**{room_name}**'"
                f"{f' (**{team_name}**)' if team_name else ''}"
            )
        )

        # base filename
        timestamp = datetime.datetime.now().strftime(constants.FILENAME_TIME_FORMAT)
        base_filename_elems = [timestamp, room_name.replace(" ", "_")]
        if team_name:
            base_filename_elems.append(team_name.replace(" ", "_"))
        backup_filename_formatted = f"{'-'.join(base_filename_elems)}.{archive_format}"
        result["filename"] = backup_filename_formatted

        # create temp workdir
        tmp_workdir = tempfile.mkdtemp()
        # change to temp dir
        os.chdir(tmp_workdir)

        # backup the space
        backup_file = wxt_archiver.archive_room(
            room_id = room,
            delete_folder=True,
            file_format="zip",
            download_workers=5
        )

        # rename the file
        shutil.move(backup_file, backup_filename_formatted)
        backup_path = os.path.join(tmp_workdir, backup_filename_formatted)

        # get file stats
        backup_stats = os.stat(backup_path)

        # send the file to the user
        backup_msg = bot_api.messages.create(
            toPersonEmail=account_mail,
            markdown=f"Backup completed: {backup_filename_formatted}",
            files=[backup_path]
        )

        result["result"] = "Success"

        task_doc.set({
            "status": "complete",
            "result": "success",
            "end_time": datetime.datetime.now(),
            "filename": backup_filename_formatted,
            "file_size": backup_stats.st_size
        }, merge=True)

    # let's notify the user in case something goes wrong
    except webexteamssdk.exceptions.ApiError as e:
        error_msg = bot_api.messages.create(
            toPersonEmail=account_mail,
            markdown=f"Error backing up room '**{room}**': {e}"
        )
        result["error_msg"] = e
        result["result"] = "Failure"

        task_doc.set({
            "status": "complete",
            "result": "failure",
            "end_time": datetime.datetime.now(),
            "error_type": "api_failure",
            "error_msg": e
        }, merge=True)

    except TaskMaxRetries as e:
        result["error_msg"] = str(e)
        result["result"] = "Failure"

        task_doc.set({
            "status": "complete",
            "result": "failure",
            "end_time": datetime.datetime.now(),
            "error_type": "max_retries",
            "error_msg": str(e)
        }, merge=True)  

    except Exception as e:
        result["error_msg"] = e
        result["result"] = "Failure"

        task_doc.set({
            "status": "complete",
            "result": "failure",
            "end_time": datetime.datetime.now(),
            "error_type": "generic_failure",
            "error_msg": e
        }, merge=True)

    finally:
        # clean things up
        try:
            shutil.rmtree(tmp_workdir)
        except Exception as e:
            print(f"Unable to clean the temp dir: {e}")

    return json.dumps(result)