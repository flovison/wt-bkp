# Webex Teams Backup (wt-bkp)

This is an implementation of a Webex Teams Integration aimed at making it easy for users to archive the content (including messages and attachments) of their Webex Teams rooms.  

## System Requirements

The application uses Flask tested with Python 3.7.  
The Webex Teams API is accessed using the following libraries:

- `webexteamssdk`
- `webexteamsarchiver`

## Webex Requirements

The application uses the credentials of a **Webex Teams Integration** in order to obtain a user token and access the user's spaces and messages.  
In addition, it also requires credentials of **Webex Teams Bot**, in order to be able to send messages to the user.  

You can register the integragion and bot at https://developer.webex.com/ .

## Configuration

The configuration is passed via the following environment variables:

- `FLASK_ENV`: e.g. set Flask in `development` mode
- `OAUTHLIB_INSECURE_TRANSPORT`: set to `1` local testing/debugging, where the OAuth `redirect_uri` is reached over `HTTP`
- `EXCLUDE_TEAMS`: If set, the tool restricts the user to see only 1:1 spaces, excluding group chats
- `WT_CLIENT_ID`: Webex Teams Integration `client_id`
- `WT_CLIENT_SECRET`: Webex Teams Integration `client_secret`
- `WT_BOT_ID`: Webex Teams Bot `client_id`
- `WT_BOT_TOKEN`: Webex Teams Bot `token`  
- `APP_SECRET`: Flask App secret (if not specified it will use a random value)
- `MAIL_DOMAINS_WHITELIST`: Comma-separated list of domains, used to restrict the use of the app to users whose Webex account matches one of the whitelsted domains. E.g. `domain1.com,domain2.org,sub.domain3.net`

## Installation

### Standalone

- Install the dependencies:  
  `pip install -r requirements.txt`
- Set the environment variables
- Start the application from the `wt-bkp` directory (where the `app.py` file is located): `flask run`

### Docker

#### Docker Hub

The image is published on Docker hub on the `federkicco/wt-bkp` repository:  

- pull the image:  
   `docker pull federkicco/wt-bkp:latest`  
- if using the current environment variables:  
  `docker run -itd --env-file config/current.env --name wt-bkp federkicco/wt-bkp`
- alternatively, store the environment variable values on a file to be referred as the `--env-file` argument.  
  See this page for more info: https://docs.docker.com/compose/env-file/ 

#### Build the docker image locally

The `Makefile` included in the project has a `build` target aimed at building the docker image, tagged using the current `git` commit hash and `latest`.  
The `Makefile` also has targets to run the image passing the current env variables: `make run-current`  

### Google App Engine

The repo is ready for deployment to `GAE`, by adding an `app.yaml` file to the `wt-bkp` directory, e.g.:

```
runtime: python37

handlers:
  # This handler routes all requests not caught above to your main app. It is
  # required when static routes are defined, but can be omitted (along with
  # the entire handlers section) when there are no static files defined.
- url: /.*
  script: auto

env_variables:
  ..put your env variables here..
```

Assuming that a project is alraedy created in Google App Engine and you already installed the `gcloud` CLI, deploy with:  
`gcloud app deploy`  

## Usage

- Once running, the first time a user accesses the root it will be redirected to Webex for authentication (OAuth flow).  
- The user has to grant permission to the integration, in order to access his/her account data (currently configured for the scope `spark:all`, it may be restricted to more specific permissions later on)
- With the returned authorization code, the app will obtain an access token allowing it to operate on the user's behalf
- The landing page `/profile` will confirm the account details in use (`Display Name` and `e-mail` address) as well as if the user is allowed to use the app or not (the current filter is only based on e-mail domain whitelisting, if the `MAIL_DOMAINS_WHITELIST` is set)
- The user with a valid account is guided to the `/myspaces` page where the tool shows the list of the available spaces that can be archived
- The user can then select one or more spaces and click on `Submit` to start the process
- The Bot will notify the user on a new 1:1 chat about the beginning of the backup operations for a given space, sending the `.zip` archive as soon as it's available
- Once all selected spaces have been processed, the user will be redirected to a status/report page to summarize the job results