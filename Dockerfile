FROM python:3.7-slim-buster

MAINTAINER Federico Lovison "federico@lovison.eu"

ARG GIT_COMMIT=unspecified
LABEL git_commit=$GIT_COMMIT

COPY requirements.txt /wt-bkp/
COPY . /wt-bkp/
WORKDIR /wt-bkp

RUN pip install -r requirements.txt

EXPOSE 5000

WORKDIR /wt-bkp/wt-bkp

CMD [ "flask", "run", "--host", "0.0.0.0" ]