NAME	:= wt-bkp
TAG		:= $$(git log -1 --pretty=%h)
IMG		:= ${NAME}:${TAG}
LATEST	:= ${NAME}:latest
 
build:
	@docker build -t ${IMG} .
	@docker tag ${IMG} ${LATEST}
 
push:
	@docker push ${NAME}
 
login:
	@docker log -u ${DOCKER_USER} -p ${DOCKER_PASS}

run-dev:
	@docker run --rm -it --env-file config/dev.env -p 5000:5000 ${NAME}

run-current:
	@docker run --rm -it --env-file config/current.env -p 5000:5000 ${NAME}