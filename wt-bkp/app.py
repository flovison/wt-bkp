from requests_oauthlib import OAuth2Session
from flask import Flask, request, redirect, session, url_for, render_template
from flask.json import jsonify
import webexteamssdk
from webexteamsarchiver import WebexTeamsArchiver
from google.cloud import firestore
from google.cloud import tasks_v2
import os
import shutil
import datetime
import tempfile
import pandas as pd
import constants
import utilities
import json
import uuid

app = Flask(__name__)
app.secret_key = os.environ.get("APP_SECRET") or os.urandom(24)
app.permanent_session_lifetime = datetime.timedelta(days=7)

# integration app credentials
client_id = os.environ.get(constants.WT_CLIENT_ID_ENV)
client_secret = os.environ.get(constants.WT_CLIENT_SECRET_ENV)
# bot credentials (used to send msgs to the user via WxT)
bot_id = os.environ.get(constants.WT_BOT_ID_ENV)
bot_token = os.environ.get(constants.WT_BOT_TOKEN_ENV)
# Webex Teams OAuth URLs
authorization_base_url = constants.WT_OAUTH_AUTHORIZE_URL
token_url = constants.WT_OAUTH_TOKEN_URL
# allow only 1:1 rooms
exclude_groups = bool(os.environ.get(constants.EXCLUDE_GROUPS_ENV))

scope_list = [
    "spark:all"
    # "spark:memberships_read",
    # "spark:kms",
    # "spark:rooms_read",
    # "spark:messages_write",
    # "spark:teams_read",
    # "spark:messages_read",
    # "spark:team_memberships_read"
]
scope_str = " ".join(scope_list)

teams_api = webexteamssdk.WebexTeamsAPI("<<no token needed for integration>>")

# Initialize firestore db
db = firestore.Client()
task_collection = db.collection(constants.TASK_COLLECTION)

# Initialize Cloud Task client
task_client = tasks_v2.CloudTasksClient()
task_queue_id = os.environ.get(constants.QUEUE_ID_ENV)
task_queue_location = os.environ.get(constants.QUEUE_LOCATION_ENV)
project_id = os.environ.get(constants.PROJECT_ID_ENV)
# tasks fully qualified queue name
task_fqqn = task_client.queue_path(project_id, task_queue_location, task_queue_id)


@app.route("/")
def index():
    """Root
    """
    # check if a token is available
    wxt_access_token = session.get("access_token")
    redirect_uri = utilities.generate_redirect_uri(request.headers['Host'])
    print(f"Redirect URI root: '{redirect_uri}'")
    # if not, trigger the auth on WxT
    if not wxt_access_token:
        print(f"Starting OAuth with client_id: '{client_id}' - scope: '{scope_str}'")
        wxt_oauth = OAuth2Session(
            client_id,
            scope=scope_str,
            redirect_uri=redirect_uri
            )
        authorization_url, state = wxt_oauth.authorization_url(authorization_base_url)

        # State is used to prevent CSRF, keep this for later.
        session['oauth_state'] = state
        return redirect(authorization_url)
    
    else:
        # if we have a valid token open the "profile" page
        return redirect(url_for('.profile'))


@app.route(constants.APP_REDIRECT_URI_ROUTE, methods=["GET"])
def callback():
    """OAuth callback, receive autz code and use it to request an access token
    """
    session.pop("access_token", None)
    args = request.args
    try:
        code = args['code']
    except KeyError:
        # if there's no autz code in the request, go back to the root
        return redirect("/")

    try:
        redirect_uri = utilities.generate_redirect_uri(request.headers['Host'])
        print(f"Requesting Token")
        print(f"Redirect URI callback: '{redirect_uri}'")
        webex_teams_access_token = teams_api.access_tokens.get(
            client_id = client_id,
            client_secret = client_secret,
            code = code,
            redirect_uri = redirect_uri
        )
    except webexteamssdk.exceptions.ApiError as e:
        print(e)
        session.pop("access_token", None)
        session.clear()
        # if the provided code is invalid, go back to the root
        return redirect("/")

    # extract the token and store it in the client session
    wxt_access_token = webex_teams_access_token.access_token
    session["access_token"] = wxt_access_token

    # init the WxT API object using the user's token
    wxt_api = webexteamssdk.WebexTeamsAPI(wxt_access_token)

    # Extract personal information and store it in the client session
    person = wxt_api.people.me()
    mail = person.emails.pop()
    session["account"] = mail
    session["person_id"] = person.id
    session["display_name"] = person.displayName

    # check if the e-mail is allowed
    allowed = utilities.mail_domain_allowed(mail)
    session["allowed"] = allowed

    print(f"Acccount '{mail}' is allowed: {allowed}")

    return redirect(url_for('.profile'))


@app.route("/profile", methods=["GET"])
def profile():
    """Fetching a protected resource using an OAuth 2 token.
    """
    wxt_access_token = session.get("access_token")
    if not wxt_access_token:
        return redirect("/")

    mail = session.get("account")
    display_name = session.get("display_name")
    allowed = session.get("allowed")

    print(f"Access to profile page by: {mail} - {display_name}")

    return render_template(
        "profile.html", 
        mail=mail, 
        display_name=display_name, 
        allowed=allowed
        )


@app.route("/myspaces", methods=["GET"])
def get_my_spaces():
    """Fetching rooms from WXT account
    """
    wxt_access_token = session.get("access_token")
    if not wxt_access_token or not session.get("allowed"):
        return redirect("/")

    # init the WxT API object using the user's token
    # todo: handle exceptions here!
    wxt_api = webexteamssdk.WebexTeamsAPI(access_token=wxt_access_token)

    display_name = session.get("display_name")
    mail = session.get("account")
    person_id = session.get("person_id")

    # retrieve a list of all rooms and teams
    all_rooms = [ room for room in wxt_api.rooms.list() ]
    rooms_df = pd.DataFrame(utilities.jsonize_rooms(all_rooms))

    spaces_data = {}

    if not rooms_df.empty:
        # compute space link
        rooms_df["space_link"] = rooms_df.room_id.apply(utilities.roomid_to_spacelink)

        # look for mail address for direct rooms
        rooms_df["member_mail"] = rooms_df[rooms_df.room_type == "direct"].room_id.apply(
            lambda dr: [ m.personEmail for m in wxt_api.memberships.list(roomId=dr) if m.personEmail != mail ][-1]
        )

        if exclude_groups:
            spaces_df = rooms_df[rooms_df.room_type == "direct"]
            print(f"Filtering out group rooms: {spaces_df.shape}")

        else:
            all_teams = [ team for team in wxt_api.teams.list() ]
            teams_df = pd.DataFrame(utilities.jsonize_teams(all_teams))

            spaces_df = rooms_df.merge(
                teams_df,
                on="team_id",
                how="left"
                )
            print(f"Considering all rooms: {spaces_df.shape}")

            # replace NaN with empty string when there's no team_name
            spaces_df.team_name.fillna(value="", inplace=True)
            
        # fill empty e-mail addresses
        spaces_df.member_mail.fillna(value="", inplace=True)

        # get a dict out of the spaces_df and extract the person's name to render the myrooms page 
        spaces_data = spaces_df.to_dict(orient="records")

    print(f"Access to rooms page by: {mail} - {display_name}")

    return render_template("myrooms.html", data=spaces_data, display_name=display_name)


# todo: remove this route
@app.route("/myteams", methods=["GET"])
def get_my_teams():
    """Fetching teams from WXT account
    """
    wxt_access_token = session.get("access_token")
    if not wxt_access_token:
        return redirect("/")

    wxt_api = webexteamssdk.WebexTeamsAPI(access_token=wxt_access_token)
    my_teams = [ r for r in wxt_api.teams.list() ]
    return json.dumps(utilities.jsonize_teams(my_teams))


@app.route("/backuptask", methods=["POST"])
def backuptask():
    """Get the list of room_ids to be backed up and process them
    """
    wxt_access_token = session.get("access_token")
    if not wxt_access_token:
        return redirect("/")

    display_name = session.get("display_name")
    person_id = session.get("person_id")

    # extract a list of room_ids from the POST form MultiDict
    rooms_requested = request.form.getlist("room_id")

    # init the API and archiver object with the user's token
    wxt_api = webexteamssdk.WebexTeamsAPI(access_token=wxt_access_token)
    wxt_archiver = WebexTeamsArchiver(wxt_access_token)
    archive_format = "zip"

    # prepare to send message to the user using a bot token
    account_mail = session.get("account")
    bot_api = webexteamssdk.WebexTeamsAPI(access_token=bot_token)

    # track results
    results = []

    for room in rooms_requested:
        # extract room data
        room_data = wxt_api.rooms.get(room)
        room_name = room_data.title

        if room_data.teamId:
            team_id = room_data.teamId
            team_data = wxt_api.teams.get(team_id)
            team_name = team_data.name
        else:
            team_id = None
            team_data = None
            team_name = None

        result = {
            "room_name": room_name,
            "team_name": team_name
        }

        # Prepare the task
        task_data = {
            'app_engine_http_request': {
                'http_method': 'POST',
                'relative_uri': '/backup_task_handler'
            }
        }  

        # prepare the task payload
        payload = {
            "person_id": person_id,
            "account_mail": account_mail,
            "access_token": wxt_access_token,
            "room_id": room,
            "room_name": room_name,
            "team_name": team_name,
            "team_id": team_id
        }

        task_payload = json.dumps(payload)
        task_payload_encoded = task_payload.encode()
        task_data['app_engine_http_request']['body'] = task_payload_encoded

        # submit the task
        response = task_client.create_task(task_fqqn, task_data)

        result["status"] = "queued"
        results.append(result)

    return render_template("backup_summary.html", data=results, display_name=display_name)


@app.route("/history")
def task_history():
    """Retrieve the task history from Firestore
    """
    wxt_access_token = session.get("access_token")
    if not wxt_access_token:
        return redirect("/")

    display_name = session.get("display_name")
    person_id = session.get("person_id")
    user_mail = session.get("account")

    tasks = task_collection.where(u'user_id', u'==', person_id).get()
    task_docs = [ t.to_dict() for t in tasks ]

    return render_template("task_history.html", data=task_docs, display_name=display_name)


@app.route("/tokenrefresh")
def logout():
    session.pop("access_token", None)
    session.clear()
    return redirect("/")
