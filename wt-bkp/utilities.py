import os
import constants
import base64
# import json
# import datetime
# from webexteamssdk.config import WEBEX_TEAMS_DATETIME_FORMAT

def generate_redirect_uri(host):
    """Use the host and OAUTHLIB_INSECURE_TRANSPORT
    to generate the OAuth redirect_uri
    """
    scheme = "https"
    # use `http://` in case of OAUTHLIB_INSECURE_TRANSPORT
    oauth_insecure = bool(os.environ.get(constants.OAUTHLIB_INSECURE_TRANSPORT_ENV))
    if oauth_insecure:
        scheme = "http"

    redirect_uri = f"{scheme}://{host}{constants.APP_REDIRECT_URI_ROUTE}"

    return redirect_uri
        

def jsonize_rooms(room_list):
    """Return a JSON version of the room list.
    Expects a list of `Room` objects from the WebexTeamsAPI.
    """
    rooms = [
        {
            "room_id": room.id,
            "room_title": room.title,
            "room_type": room.type,
            "team_id": room.teamId,
            "last_activity": str(room.lastActivity)
        }
        for room in room_list
    ]

    return rooms


def jsonize_teams(teams_list):
    """Return a JSON version of the teams list.
    Expects a list of `Team` objects from the WebexTeamsAPI.
    """
    teams = [
        {
            "team_id": team.id,
            "team_name": team.name
        }
        for team in teams_list
    ]

    return teams


def mail_domain_allowed(mail_address):
    """Checks if there's a configured e-mail domain whitelist.
    If there is, returns `True` if the user's mail address domain 
    matches the whitelist, or `False` otherwise.
    """
    # get the whitelist domains from env variable, comma separated
    allowed_domains = os.environ.get(constants.MAIL_DOMAINS_WHITELIST_ENV)
    if allowed_domains:
        allowed_domains_list = allowed_domains.split(",")
        print(f"Mail domain whitelist: {allowed_domains_list}")
        user_domain = mail_address.split("@")[-1]
        if user_domain in allowed_domains_list:
            return True
        else:
            return False

    # if no whitelist configured, everyone is allowed
    else:
        return True


def roomid_to_spacelink(id):
    """Given a `roomid`, return a link to a Webex Teams
    space using the `webexteams` protocol handler
    """
    space_link_cs = base64.b64decode(id).decode("utf-8")
    space_id = space_link_cs.split("/")[-1]
    space_link_wt = f"webexteams://im?space={space_id}"

    return space_link_wt
